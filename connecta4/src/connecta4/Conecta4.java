package connecta4;

import java.util.Scanner;

/**
@author      Sergi Gomez &lt;sgomezf@ies-sabadell.cat&gt;
@version     1.6                 (current version number of program)
@since       1.2          (the version of the package this class was first added to)
*/


public class Conecta4 {
    static String[][] tauler;
    static int files;
    static int columnes;
    static String jugador1;
    static String jugador2;
    static int jugades;
    static String torn;

    /**
     * Metode principal que s'executa en iniciar el programa. Controla el flux del joc.
     * @param args Arguments de linia de comandes (no s'utilitzen en aquest cas)
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int opcio;

        do {
            System.out.println("1.- Mostrar Ajuda");
            System.out.println("2.- Opcions");
            System.out.println("3.- Jugar Partida");
            System.out.println("4.- Veure Ranking");
            System.out.println("0.- Sortir");
            System.out.print("Selecciona una opcio: ");
            opcio = scanner.nextInt();

            switch (opcio) {
                case 1:
                    mostrarAjuda();
                    break;
                case 2:
                    modificarOpcions(scanner);
                    break;
                case 3:
                    jugarPartida(scanner);
                    break;
                case 4:
                    veureRanking();
                    break;
                case 0:
                    System.out.println("Gracies per jugar! Fins aviat!");
                    break;
                default:
                    System.out.println("Opcio invalida. Si us plau, selecciona una opcio valida.");
                    break;
            }
        } while (opcio != 0);
    }

    /**
     * Mostra la informacio d'ajuda del joc Connecta 4.
     */
    public static void mostrarAjuda() {
        System.out.println("Benvingut a Connecta 4!");
        System.out.println("L'objectiu del joc es connectar quatre fitxes en línia: vertical, horitzontal o diagonal.");
        System.out.println("Els jugadors alternaven els seus torns i col·loquen una fitxa en una columna del tauler.");
        System.out.println("La fitxa cau fins a la posicio mes baixa disponible en aquesta columna.");
        System.out.println("Guanya el jugador que aconsegueixi connectar quatre fitxes primer.");
        System.out.println("Que comenci la diversio!");
    }

    /**
     * Permet modificar les opcions del joc, com ara els noms dels jugadors i les dimensions del tauler.
     * @param scanner Objecte Scanner per llegir les dades d'entrada de l'usuari
     */
    public static void modificarOpcions(Scanner scanner) {
        System.out.print("Introdueix el nom del primer jugador: ");
        jugador1 = scanner.next();
        System.out.print("Introdueix el nom del segon jugador: ");
        jugador2 = scanner.next();
        System.out.print("Introdueix el nombre de files del tauler: ");
        files = scanner.nextInt();
        System.out.print("Introdueix el nombre de columnes del tauler: ");
        columnes = scanner.nextInt();
    }

    /**
     * Inicialitza el tauler del joc amb les dimensions especificades.
     */
    public static void inicialitzarTauler() {
        tauler = new String[files][columnes];
        for (int i = 0; i < files; i++) {
            for (int j = 0; j < columnes; j++) {
                tauler[i][j] = " ";
            }
        }
    }

    /**
     * Imprimeix el tauler actual del joc.
     */
    public static void imprimirTauler() {
        for (int i = 0; i < files; i++) {
            for (int j = 0; j < columnes; j++) {
                System.out.print("| " + tauler[i][j] + " ");
            }
            System.out.println("|");
        }
        for (int j = 0; j < columnes; j++) {
            System.out.print("====");
        }
        System.out.println("=");
        for (int j = 0; j < columnes; j++) {
            System.out.printf("  %d ", j + 1);
        }
        System.out.println();
    }

    /**
     * Comprova si una tirada en una determinada columna es valida.
     * @param columna La columna en la qual es vol realitzar la tirada
     * @return Cert si la tirada es valida, fals altrament
     */
    public static boolean esTiradaValida(int columna) {
        return columna >= 0 && columna < columnes && tauler[0][columna].equals(" ");
    }

    /**
     * Realitza una tirada en una determinada columna.
     * @param columna La columna en la qual es vol realitzar la tirada
     */
    public static void realitzarTirada(int columna) {
        for (int i = files - 1; i >= 0; i--) {
            if (tauler[i][columna].equals(" ")) {
                tauler[i][columna] = torn;
                jugades++;
                break;
            }
        }
    }

    /**
     * Verifica si hi ha un guanyador en el tauler actual.
     * @return Cert si hi ha un guanyador, fals altrament
     */
    public static boolean verificarGuanyador() {
        // Comprovar files
        for (int i = 0; i < files; i++) {
            for (int j = 0; j < columnes - 3; j++) {
                String fitxa = tauler[i][j];
                if (!fitxa.equals(" ") && fitxa.equals(tauler[i][j + 1]) && fitxa.equals(tauler[i][j + 2]) && fitxa.equals(tauler[i][j + 3])) {
                    return true;
                }
            }
        }

        // Comprovar columnes
        for (int j = 0; j < columnes; j++) {
            for (int i = 0; i < files - 3; i++) {
                String fitxa = tauler[i][j];
                if (!fitxa.equals(" ") && fitxa.equals(tauler[i + 1][j]) && fitxa.equals(tauler[i + 2][j]) && fitxa.equals(tauler[i + 3][j])) {
                    return true;
                }
            }
        }

        // Comprovar diagonal ascendent
        for (int i = 3; i < files; i++) {
            for (int j = 0; j < columnes - 3; j++) {
                String fitxa = tauler[i][j];
                if (!fitxa.equals(" ") && fitxa.equals(tauler[i - 1][j + 1]) && fitxa.equals(tauler[i - 2][j + 2]) && fitxa.equals(tauler[i - 3][j + 3])) {
                    return true;
                }
            }
        }

        // Comprovar diagonal descendent
        for (int i = 0; i < files - 3; i++) {
            for (int j = 0; j < columnes - 3; j++) {
                String fitxa = tauler[i][j];
                if (!fitxa.equals(" ") && fitxa.equals(tauler[i + 1][j + 1]) && fitxa.equals(tauler[i + 2][j + 2]) && fitxa.equals(tauler[i + 3][j + 3])) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Comprova si el tauler esta ple (totes les caselles son ocupades).
     * @return Cert si el tauler esta ple, fals altrament
     */
    public static boolean esTaulerPlen() {
        return jugades == files * columnes;
    }

    /**
     * Mostra el ranking dels jugadors guanyadors.
     */
    public static void veureRanking() {
        System.out.println("Ranking de jugadors guanyadors:");
        // Verificar si s'han guardat jugadors guanyadors
        if (jugador1 == null && jugador2 == null) {
            System.out.println("Encara no hi ha jugadors guanyadors.");
        } else {
            if (jugador1 != null) {
                System.out.println(jugador1);
            }
            if (jugador2 != null) {
                System.out.println(jugador2);
            }
        }
    }

    /**
     * Inicia una partida del joc Connecta 4.
     * @param scanner L'objecte Scanner per llegir l'entrada de l'usuari
     */
    public static void jugarPartida(Scanner scanner) {
        if (jugador1 == null) {
            jugador1 = "Jugador 1";
        }
        if (jugador2 == null) {
            jugador2 = "Jugador 2";
        }
        if (files <= 0) {
            files = 6;
        }
        if (columnes <= 0) {
            columnes = 7;
        }

        inicialitzarTauler();
        jugades = 0;
        torn = "X";

        boolean partidaAcabada = false;
        while (!partidaAcabada) {
            imprimirTauler();
            System.out.printf("%s, es el teu torn! Introdueix el número de columna: ", torn.equals("X") ? jugador1 : jugador2);
            int columna = scanner.nextInt() - 1;

            if (esTiradaValida(columna)) {
                realitzarTirada(columna);
                if (verificarGuanyador()) {
                    imprimirTauler();
                    System.out.printf("Enhorabona, %s! Has guanyat la partida!\n", torn.equals("X") ? jugador1 : jugador2);
                    partidaAcabada = true;
                } else if (esTaulerPlen()) {
                    imprimirTauler();
                    System.out.println("Empat! La partida ha acabat en empat.");
                    partidaAcabada = true;
                } else {
                    torn = torn.equals("X") ? "O" : "X";
                }
            } else {
                System.out.println("Tirada invalida. Si us plau, selecciona una columna valida.");
            }
        }
    }
}
