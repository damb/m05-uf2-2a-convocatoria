package connecta4;

import static org.junit.Assert.*;
import org.junit.*;

public class Conecta4Test {
  
  @Test
  public void testEsTiradaValida_ColumnaValida_True() {
    Conecta4.inicialitzarTauler();
    boolean result = Conecta4.esTiradaValida(3);
    assertTrue(result);
  }
  
  @Test
  public void testEsTiradaValida_ColumnaInvalida_False() {
    Conecta4.inicialitzarTauler();
    boolean result = Conecta4.esTiradaValida(-1);
    assertFalse(result);
  }
  
  @Test
  public void testRealitzarTirada_TiradaValida_TaulerActualitzat() {
    Conecta4.inicialitzarTauler();
    Conecta4.realitzarTirada(2);
    String expected = "X";
    String actual = Conecta4.tauler[Conecta4.files - 1][2];
    assertEquals(expected, actual);
  }
  
  @Test
  public void testVerificarGuanyador_GuanyadorEnFiles_True() {
    Conecta4.inicialitzarTauler();
    Conecta4.realitzarTirada(0);
    Conecta4.realitzarTirada(1);
    Conecta4.realitzarTirada(0);
    Conecta4.realitzarTirada(1);
    Conecta4.realitzarTirada(0);
    Conecta4.realitzarTirada(1);
    Conecta4.realitzarTirada(0);
    boolean result = Conecta4.verificarGuanyador();
    assertTrue(result);
  }
  
  @Test
  public void testEsTaulerPlen_TaulerNoPlen_False() {
    Conecta4.inicialitzarTauler();
    boolean result = Conecta4.esTaulerPlen();
    assertFalse(result);
  }
  
}
